"""artscraper_dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
import views

urlpatterns = [
    url(r'^scrape_links/', views.scrape_links),
    url(r'^get_screenshots', views.scrape_screens),
    url(r'^get_background_color', views.get_background_color),
    url(r'^get_height_map', views.get_height_map),
    url(r'^generate_islands', views.generate_islands),
    url(r'^get_island_grid_artforum', views.get_island_grid_artforum),
    url(r'^get_island_grid_alexa', views.get_island_grid_alexa),
    url(r'^scrape_fonts', views.scrape_fonts),
    url(r'^generate_fragments', views.generate_fragments),
    url(r'^get_least_used_background_color', views.get_least_used_background_color),
    url(r'^get_max_height', views.get_max_height),
    url(r'^get_opposite_web_artforum', views.get_opposite_web_artforum),
    url(r'^get_opposite_web_alexa', views.get_opposite_web_alexa),
    url(r'^get_sentences', views.get_sentences),
    url(r'^get_least_used_font_alexa', views.get_least_used_font_alexa),
    url(r'^get_least_used_font_artforum', views.get_least_used_font_artforum),
    url(r'^dump_sentences_artforum', views.dump_sentences_artforum),
    url(r'^dump_sentences_alexa', views.dump_sentences_alexa),
    url(r'^parse_markov', views.parse_markov),
    url(r'^generate_markov', views.generate_markov),
    url(r'^get_least_common_sentences_artforum', views.get_least_common_sentences_artforum),
    url(r'^get_least_common_sentences_alexa', views.get_least_common_sentences_alexa),

]
