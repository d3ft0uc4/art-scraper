import os
import codecs
from selenium import webdriver
import time
import json
import numpy as np

t1 = int(time.time())
wd = webdriver.PhantomJS()
script_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'main.js')

with codecs.open(script_path, 'r', encoding='utf-8') as f:
    source = f.read()
    wd.execute_script(source)
    print 'script loaded in {0} secs'.format(str(int(time.time()) - t1))


def generate(url):
    return str(wd.execute_script("return window['generator'](arguments[0])", url))
