import cv2
import numpy as np
from artscraper_dj.models import Segment


def make_segments(screen_obj, tpe):
    image = cv2.imdecode(np.frombuffer(screen_obj.screenshot, np.uint8), 1)
    edged = cv2.Canny(image, 10, 250)
    (_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    idx = 0
    segments = []
    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        # if w > 30 and h > 30:
        if w > 5 and h > 5:
            idx += 1
            new_img = image[y:y + h, x:x + w]
            position_x = x + w / 2
            position_y = y + h / 2
            r, buf = cv2.imencode('.png', new_img)
            segments.append(
                Segment(position_x=position_x, position_y=position_y, segment=bytearray(buf), screenshot=screen_obj,
                        width=w, height=h, type=tpe))
    for s in segments:
        s.save()
