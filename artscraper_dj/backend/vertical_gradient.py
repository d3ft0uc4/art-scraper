from PIL import Image


def add_gradient(im, alpha=0.4):
    gradient = Image.new('L', (1, 255))
    for y in range(255):
        gradient.putpixel((0, 254 - y), y)
    # resize the gradient to the size of im...
    gradient = gradient.resize(im.size).convert('RGB')
    return Image.blend(im, gradient, alpha)
