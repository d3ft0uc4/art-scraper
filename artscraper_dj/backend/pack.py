#!/usr/bin/env python
# -*- coding: utf-8 -*-
from PIL import Image, ImageDraw, ImageFont
import random
from random import randint
import copy
import PIL

random.seed()


# SUBDIVIDE

class Point(object):
    def __init__(self, x, y):
        self.x, self.y = x, y

    @staticmethod
    def from_point(other):
        return Point(other.x, other.y)


class Rect(object):
    def __init__(self, x1, y1, x2, y2):
        minx, maxx = (x1, x2) if x1 < x2 else (x2, x1)
        miny, maxy = (y1, y2) if y1 < y2 else (y2, y1)
        self.min, self.max = Point(minx, miny), Point(maxx, maxy)

    @staticmethod
    def from_points(p1, p2):
        return Rect(p1.x, p1.y, p2.x, p2.y)

    width = property(lambda self: self.max.x - self.min.x)
    height = property(lambda self: self.max.y - self.min.y)


def plus_or_minus(input):
    nums = ['1', '-1']
    input = input * int(random.choice(nums))
    return input


def quadsect(rect, factor):
    """ Subdivide given rectangle into four non-overlapping rectangles.
        'factor' is an integer representing the proportion of the width or
        height the deviatation from the center of the rectangle allowed.
    """
    # pick a point in the interior of given rectangle
    w, h = rect.width, rect.height  # cache properties
    center = Point(rect.min.x + (w // 2), rect.min.y + (h // 2))
    delta_x = plus_or_minus(randint(0, w // factor))
    delta_y = plus_or_minus(randint(0, h // factor))
    interior = Point(center.x + delta_x, center.y + delta_y)

    # create rectangles from the interior point and the corners of the outer
    # one
    return [Rect(interior.x, interior.y, rect.min.x, rect.min.y),
            Rect(interior.x, interior.y, rect.max.x, rect.min.y),
            Rect(interior.x, interior.y, rect.max.x, rect.max.y),
            Rect(interior.x, interior.y, rect.min.x, rect.max.y)]


def square_subregion(rect):
    """ Return a square rectangle centered within the given rectangle """
    w, h = rect.width, rect.height  # cache properties
    if w < h:
        offset = (h - w) // 2
        return Rect(rect.min.x, rect.min.y + offset,
                    rect.max.x, rect.min.y + offset + w)
    else:
        offset = (w - h) // 2
        return Rect(rect.min.x + offset, rect.min.y,
                    rect.min.x + offset + h, rect.max.y)


data = None


def populate_data(im):
    width, height = im.size
    xDiv = width / 600
    units = width / xDiv
    print units
    yDiv = height / units
    print units, " ", yDiv

    NUM_RECTS = xDiv * yDiv
    REGION = Rect(0, 0, width, height)
    # call quadsect() until at least the number of rects wanted has been generated
    rects = [REGION]  # seed output list
    while len(rects) <= NUM_RECTS:
        rects = [subrect for rect in rects
                 for subrect in quadsect(rect, 3)]

    random.shuffle(rects)  # mix them up
    sample = random.sample(rects, NUM_RECTS)  # select the desired number
    print '%d out of the %d rectangles selected' % (NUM_RECTS, len(rects))
    # SUBDIV OVER
    return {'width': width, 'height': height, 'xDiv': xDiv, 'units': units, 'yDiv': yDiv, 'NUM_RECTS': NUM_RECTS,
            'REGION': REGION, 'sample': sample}


def run(im):
    # slice image
    global data
    if data is None:
        data = populate_data(im)
    image_list = []
    centers = []
    for x in range(0, data['xDiv']):
        xPos = x * (data['width'] / data['xDiv'])
        for y in range(0, data['yDiv']):
            yPos = y * (data['height'] / data['yDiv'])
            box = (xPos, yPos, xPos + data['units'], yPos + data['units'])
            region = im.crop(box)
            image_list.append(region)
    print image_list
    # Draw new map
    imgx, imgy = data['REGION'].max.x + 1, data['REGION'].max.y + 1
    imageOut = Image.new("RGBA", (imgx, imgy))
    i = 0
    for rect in data['sample']:
        newSizeX = square_subregion(rect).max.x - square_subregion(rect).min.x
        newSizeY = square_subregion(rect).max.y - square_subregion(rect).min.y
        center = (square_subregion(rect).min.x - (newSizeX / 2), square_subregion(rect).min.y - (newSizeY / 2))
        centers.append(center)
        im2 = image_list[i].copy()
        im2 = im2.resize((newSizeX, newSizeY))
        imageOut.paste(im2, (square_subregion(rect).min.x, square_subregion(rect).min.y))
        i += 1
    imageOut = imageOut.resize((data['width'], data['height']))
    return imageOut
