import numpy as np
import scipy
import scipy.ndimage
from PIL import Image


# Define the make_turbulence function, which will create noise patterns
def make_turbulence(imsize_y, imsize_x):
    # Initialize the white noise pattern
    base_pattern = np.random.uniform(0, 255, (imsize_x // 2, imsize_y // 2))

    # Initialize the output pattern
    turbulence_pattern = np.zeros((imsize_x, imsize_y))

    # Create cloud pattern
    power_range = range(2, int(np.log2(max([imsize_x, imsize_y]))))
    for i in power_range:
        # Set the size of the quadrant to work on
        subimg_size = 2 ** i

        # Extract the pixels in the upper left quadrant
        quadrant = base_pattern[:subimg_size, :subimg_size]

        # Up-sample the quadrant to the original image size
        # intperp can be 'nearest', 'lanczos', 'bilinear', 'bicubic' or 'cubic'
        upsampled_pattern = scipy.misc.imresize(quadrant, (imsize_x, imsize_y), interp='bicubic')

        # Add the new noise pattern to the result
        turbulence_pattern += upsampled_pattern / subimg_size

    # Normalize values
    turbulence_pattern /= sum([1.0 / 2 ** i for i in power_range])

    return turbulence_pattern


def add_clouds(img, alpha=0.5):
    turbulence_pattern = make_turbulence(img.size[0], img.size[1])
    turb_img = Image.fromarray(np.dstack([turbulence_pattern.astype(np.uint8)] * 3))
    return Image.blend(img, turb_img, alpha)



