from PIL import Image
import io

height = 600
width = 600


def mapper(height_map, rgb_palette):
    assert len(rgb_palette) == 10
    img = Image.new('RGBA', (width, height))
    pixels = img.load()
    for y, row in enumerate(height_map):
        for x, h in enumerate(row):
            if h > threshold[9]:
                r, g, b = rgb_palette[9]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[8]:
                r, g, b = rgb_palette[8]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[7]:
                r, g, b = rgb_palette[7]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[6]:
                r, g, b = rgb_palette[6]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[5]:
                r, g, b = rgb_palette[5]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[4]:
                r, g, b = rgb_palette[4]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[3]:
                r, g, b = rgb_palette[3]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[2]:
                r, g, b = rgb_palette[2]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[1]:
                r, g, b = rgb_palette[1]
                pixels[x, y] = (r, g, b, 255)
            elif h > threshold[0]:
                r, g, b = rgb_palette[0]
                pixels[x, y] = (r, g, b, 255)
            else:
                pixels[x, y] = (255, 255, 255, 0)

    imgByteArr = io.BytesIO()
    img.save(imgByteArr, format='PNG')
    return imgByteArr.getvalue()


# RULES
threshold = [0.08, 0.12, 0.15, 0.2, 0.25, 0.3, 0.4, 0.45, 0.55, 0.6]
