from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import matplotlib
from PIL import Image
import time

font = {'family': 'DejaVu Sans',
        'weight': 'normal',
        'size': 0.5}

matplotlib.rc('font', **font)


def get_mollweide(pic):
    m = Basemap(projection='moll', lon_0=0, resolution='c')
    m.warpimage(pic)
    plt.savefig('output-warped-{0}.pdf'.format(str(int(time.time()))), format='pdf', dpi=600)


# img = Image.open('output-combined.png')
# img = img.resize((img.size[0] / 4, img.size[1] / 4))
# img.save('output-resized.png')
# get_mollweide('../../output-resized-1506351667.png')
