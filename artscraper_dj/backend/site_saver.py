from selenium import webdriver
from artscraper_dj.models import Screenshot, Sentence
import traceback
from artscraper_dj.models import BackgroundColor
import logging
from bs4 import BeautifulSoup
from bs4.element import Comment
from urllib import urlopen

# window width/height
width = 1600
height = 1200

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def get_screenshot(link_obj, first=True):
    print 'current url', link_obj.url
    if first:
        driver = webdriver.PhantomJS()
    else:
        driver = webdriver.Chrome()
    try:
        driver.set_window_size(width, height)
        driver.get(link_obj.url)
        screenshot_binary = driver.get_screenshot_as_png()
        Screenshot(link=link_obj, screenshot=screenshot_binary).save()
    except Exception as e:
        traceback.print_exc(10)
        if first:
            driver.close()
            get_screenshot(link_obj, False)
    finally:
        driver.quit()


def get_background_color(link_obj):
    print 'current url', link_obj.url
    driver = webdriver.PhantomJS()
    try:
        driver.set_window_size(width, height)
        driver.get(link_obj.url)
        color_str = driver.find_element_by_tag_name('body').value_of_css_property('background-color')
        r, g, b, a = map(int, color_str.replace('rgba(', '').replace(')', '').split(','))
        print (r, g, b, a)
        BackgroundColor(link=link_obj, background=str([r, g, b, a])).save()
    except Exception as e:
        traceback.print_exc(10)
        BackgroundColor(link=link_obj, background=str([255, 255, 255, 0])).save()
    finally:
        driver.quit()


# def get_screenshot_notext(url):
#     try:
#         driver.get(url)
#         driver.implicitly_wait(10)
#         elems = driver.find_elements_by_css_selector('*')
#         for elem in elems:
#             try:
#                 if elem.text is not '' and elem.text is not None:
#                     driver.execute_script("arguments[0].style.color = 'transparent';", elem)
#             except StaleElementReferenceException:  # Element can be removed from DOM while we iterating
#                 pass
#         fname = get_url_fname(url) + '-notext.png'
#         path = os.path.join(screenshot_dir, fname)
#         driver.save_screenshot(path)
#         return path
#     except:
#         traceback.print_exc(limit=10)


def get_sentences(link):
    driver = webdriver.PhantomJS()
    try:
        driver.set_window_size(width, height)
        driver.get(link.url)
        log.debug('Current link: {0}'.format(link.url))
        elems = driver.find_elements_by_css_selector('*')
    except Exception as e:
        log.error('Failed link - id:{0} , url:{1}'.format(link.id, link.url))
        driver.close()
        return
    for elem in elems:
        try:
            if not len(elem.find_elements_by_xpath('.//*')):
                if elem.text is not None:
                    if str(unicode(elem.text).encode('utf-8')):
                        log.debug('Found text: {0}'.format(str(unicode(elem.text).encode('utf-8'))))
                        location = elem.location
                        Sentence(link=link, text=str(unicode(elem.text).encode('utf-8')), pos_x=location['x'],
                                 pos_y=location['y'], type=link.type).save()
        except:
            traceback.print_exc()
            log.error('Failed link - id:{0} , url:{1}'.format(link.id, link.url))
    driver.close()


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True

# def get_sentences(link):
#     body = urlopen(link.url).read()
#     soup = BeautifulSoup(body, 'html.parser')
#     texts = soup.findAll(text=True)
#     visible_texts = filter(tag_visible, texts)
#     return u" ".join(t.strip() for t in visible_texts)
