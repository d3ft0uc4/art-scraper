import requests
from bs4 import BeautifulSoup

import alexa


OVERWRITE_LINK = False


def get_alexa_top_links(count):
    return alexa.top_list(count)


def get_art_links(forum_url):
    links = []
    previous_length = 0
    i = 0
    while True:
        current_url = forum_url.format(i)
        page = requests.get(current_url, verify=False)
        soup = BeautifulSoup(page.content, 'html.parser')
        for addr in soup.find_all('p', {'class': 'Address'}):
            links.append(addr.find('a')['href'])
        current_length = len(list(set(links)))
        if current_length == previous_length:
            break
        else:
            previous_length = current_length
        i += 1
    return links



