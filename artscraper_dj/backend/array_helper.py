import copy
import itertools
import math
from PIL import Image, ImageDraw, ImageFilter, ImageFont
import json
import traceback
import time

from artscraper_dj.backend import pack


def product(nums):
    return list(itertools.product(*((x, -x) for x in nums)))


def get_neighbours(m):
    nums = list(itertools.combinations_with_replacement(range(0, m + 1), r=2))
    reversed_nums = []
    for a, b in nums:
        reversed_nums.append((b, a))
    return set(reduce(lambda x, y: x + y, map(product, nums + reversed_nums)))


def get_dimensions(n):
    y = int(math.ceil(math.sqrt(math.ceil(float(n) / 2))))
    x = int(2 * y)
    return x, y


def fill(m, n):
    return [x[:] for x in [[0] * m] * n]


def get_walk(num):
    m, n = get_dimensions(num)
    j = (m / 2)
    i = (n / 2)
    array = fill(m, n)
    counter = 0
    for d in xrange(max(m, n)):
        for a, b in get_neighbours(d):
            try:
                if array[i + a][j + b] == 0:
                    counter += 1
                    array[i + a][j + b] = counter
            except IndexError:
                continue
    return array


def concatenate_backgrounds(x, y, walk, sorted_backgrounds, links):
    width = 600
    height = 600
    new_im = Image.new('RGB', (width * x, height * y), color=(255, 255, 255, 0))
    matrix = copy.deepcopy(walk)
    for i in xrange(0, width * x, width):
        for j in xrange(0, height * y, height):
            a = i / 600
            b = j / 600
            try:
                p = walk[b][a]
                if p <= len(sorted_backgrounds):
                    color = tuple(json.loads(sorted_backgrounds[p - 1]))
                    (red, green, blue, alpha) = color
                    if alpha is 0:
                        red = 255
                        green = 255
                        blue = 255
                    draw = ImageDraw.Draw(new_im)
                    draw.rectangle(((i, j), (i + 600, j + 600)), fill=(red, green, blue))
                    del draw
                    l = links[p - 1]
                    matrix[b][a] = '{0} || {1}'.format(l.url, str(l.rank))
                else:
                    matrix[b][a] = ' '
            except Exception as e:
                traceback.print_exc(10)

    with open('matrix.json', 'w') as outfile:
        json.dump(matrix, outfile)
    new_im = pack.run(new_im)
    new_im = new_im.filter(ImageFilter.GaussianBlur(radius=50))
    output = 'background-tmp-{0}.png'.format(str(int(time.time())))
    new_im.save(output)
    return new_im


def concatenate_islands(x, y, walk, sorted_islands, sorted_links,store_keys=False):
    width = 600
    height = 600
    new_im = Image.new('RGBA', (width * x, height * y), color=(255, 255, 255, 0))
    if store_keys:
        cnter = 1
        d = {}
    for i in xrange(0, width * x, width):
        for j in xrange(0, height * y, height):
            a = i / 600
            b = j / 600
            try:
                p = walk[b][a]
                if p <= len(sorted_islands):
                    from cStringIO import StringIO
                    island = Image.open(StringIO(sorted_islands[p - 1]))
                    if store_keys:
                        d[cnter] = {'url': sorted_links[p - 1].url, 'rank': sorted_links[p - 1].rank}
                        draw = ImageDraw.Draw(island)
                        font = ImageFont.truetype(font='Arial', size=300)
                        draw.text((100, 100), str(cnter), (0, 0, 0, 255), font=font)
                        del draw
                        cnter += 1
                    new_im.paste(island, (i, j))
            except Exception as e:
                traceback.print_exc(10)
    output = 'islands-tmp-{0}.png'.format(str(int(time.time())))
    new_im.save(output)
    if store_keys:
        with open('url-mapping-{0}.json'.format(str(int(time.time()))), 'w') as out:
            json.dump(d, out)
    return new_im


def combine_img(background, foreground):
    img = Image.alpha_composite(background.convert('RGBA'), foreground)
    img.save('for-test-{0}.png'.format(str(int(time.time()))))
    return img
