# import cv2
# # from PIL import Image
# import numpy as np
# import os
# from pprint import pprint
# import codecs
# import json
# from PIL import Image, ImageDraw
#
#
# # set for center of all contours detected
# # coordinates = set()
# # imageFiles = []
#
#
# def make_segments(screen_obj):
#     rgb = cv2.imdecode(np.frombuffer(screen_obj.screenshot, np.uint8), 1)
#     # get image size
#     height, width, channels = rgb.shape
#     # create black image for alpha
#     black = np.zeros([height, width, 3], dtype=np.uint8)
#     src = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
#
#     # FIND CONTOURS
#     src = cv2.medianBlur(src, 21)
#     # Set threshold and maxValue
#     thresh = 150
#     maxValue = 255
#     # Basic threshold example
#     th, dst = cv2.threshold(src, thresh, maxValue, cv2.THRESH_BINARY)
#     # closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
#     # Find Contours
#     img, countours, hierarchy = cv2.findContours(
#         dst, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
#
#     # Draw Contour
#     cv2.drawContours(rgb, countours, -1, (255, 255, 255), 2)
#     # cv2.imwrite("shapes.png", rgb)
#     cv2.imwrite(path + "/shapes2.png", rgb2)
#
#     # Make mask dir
#     if not os.path.exists("mask"):
#         os.makedirs("mask")
#
#     cv2.drawContours(black, countours, -1, (255, 255, 255), -1)
#     black = cv2.GaussianBlur(black, (5, 5), 0)
#     alpha_channel = cv2.cvtColor(black, cv2.COLOR_BGR2GRAY)
#     b_channel, g_channel, r_channel = cv2.split(rgb2)
#     img_RGBA = cv2.merge((b_channel, g_channel, r_channel, alpha_channel))
#     cv2.imwrite(path + "/contours.png", img_RGBA)
#
#     # MAKE INDIVIDUAL CONTOURS
#     count = 0
#     angular = 0
#     soft = 0
#     total = 0
#     contourList = list()
#     imagesList = list()
#     print "Looking through contours:\n",
#
#     # save conoutors
#     for index, cnt in enumerate(countours):
#         area = cv2.contourArea(cnt)
#         if area > 1000:
#             approx = cv2.approxPolyDP(
#                 cnt, 0.01 * cv2.arcLength(cnt, True), True)
#             if len(approx) < 10:
#                 angular += 1
#                 # print count, "A", angular,
#             else:
#                 soft += 1
#                 # print count, "S", soft,
#             # save contour
#             # print " area: ", area,
#             black2 = np.zeros([height, width, 3], dtype=np.uint8)
#             cv2.drawContours(
#                 black2, countours, index, (255, 255, 255), -1, 8, hierarchy)
#             black = cv2.GaussianBlur(black2, (5, 5), 0)
#             alpha_channel = cv2.cvtColor(black2, cv2.COLOR_BGR2GRAY)
#             b_channel, g_channel, r_channel = cv2.split(rgb2)
#             img2_RGBA = cv2.merge((b_channel,
#                                    g_channel,
#                                    r_channel,
#                                    alpha_channel))
#             # centers
#             M = cv2.moments(cnt)
#             cX = int(M["m10"] / M["m00"])
#             cY = int(M["m01"] / M["m00"])
#             coordinates = (cX, cY)
#
#             # crop images
#             x, y, w, h = cv2.boundingRect(cnt)
#             # print(x, y, w, h)
#             crop_img = img2_RGBA[y: y + h, x: x + w]
#             _h, _w = crop_img.shape[:2]
#             print 'Width:{0},Height:{1}'.format(_w, _h)
#             print 'Count {0}'.format(str(count))
#             cv2.imwrite(path + "/" + str(count) + ".png", crop_img)
#             count += 1
#             total += len(approx)
