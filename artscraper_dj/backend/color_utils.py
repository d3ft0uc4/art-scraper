import cv2
import numpy as np


def dominant_colors(obj):
    rgb = cv2.imdecode(np.frombuffer(obj.screenshot, np.uint8), 1)
    tinyrgb = cv2.resize(rgb, (0, 0), fx=0.1, fy=0.1)
    Z = tinyrgb.reshape((-1, 3))
    # convert to np.float32
    Z = np.float32(Z)
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 10
    L = np.array([])
    ret, label, center = cv2.kmeans(
        Z, K, L, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    # Now convert back into uint8, and make original image
    return np.uint8(center).astype('int').tolist()
