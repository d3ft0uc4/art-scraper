import cookielib
import re
import urllib2
from bs4 import BeautifulSoup
import json
from artscraper_dj.models import Fonts

# url_list = ["https://76crimes.com/2014/06/25/zambia-to-west-dont-watch-when-we-jail-lgbt-people/"]

pattern = re.compile(r'font-family:([^;|}]+)')
generic_font = ["serif", "sans-serif", "cursive", "fantasy", "monospace", "inherit", "sans-serf"]
generic_font.extend(map(lambda x: x + '!important', generic_font))
generic_font.extend(map(lambda x: x + ' !important', generic_font))


def get_fonts(link_obj):
    cj = cookielib.CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    url = link_obj.url
    print 'Parsing link: ', url
    try:
        req = urllib2.Request(url)
        req.add_header('User-agent', 'Mozilla/5.0')
        response = opener.open(req).read()
        soup = BeautifulSoup(response, 'lxml')
        sheets = []
        fonts = []
        for styletag in soup.findAll('style'):
            if not styletag.string:
                continue
            sheets.append(styletag.string)

        css_links = [link["href"] for link in soup.findAll("link") if "stylesheet" in link.get("rel", [])]
        for link in css_links:
            try:
                if link.startswith("//"):
                    link = link[2:]
                if 'http://' not in link and 'https://' not in link and link.startswith("/"):
                    link = url + '/' + link
                    if not link.startswith("http"):
                        link = "https://" + link
                req = urllib2.Request(link)
                req.add_header('User-agent', 'Mozilla/5.0')
                response = opener.open(link).read()
                sheets.append(response)
            except Exception as e:
                print(e, link)
        for sheet in sheets:
            try:
                f = pattern.findall(sheet)
                for occurrence in f:
                    l = occurrence.split(",")
                    for font in l:
                        if not any(n in font for n in generic_font):
                            fonts.append(font.replace('"', '').replace("'", "").replace("!important", '').strip())
            except:
                pass
        if fonts:
            Fonts(link=link_obj, font=json.dumps(list(set(fonts)))).save()
    except Exception as e:
        print(e, url)
        pass

# get_fonts('https://76crimes.com/2014/06/25/zambia-to-west-dont-watch-when-we-jail-lgbt-people/')
