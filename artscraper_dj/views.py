import random
import traceback
from multiprocessing.pool import ThreadPool
from PIL import ImageFont
from PIL import ImageDraw
from django.http import HttpResponse
from artscraper_dj.backend import site_saver
from artscraper_dj.backend.segments import make_segments
from backend.scrape_utils import *
import time
from models import *
from backend import color_utils, array_helper
import operator
from backend.colormap import *
import json
from backend.vertical_gradient import *
from backend.clouds import *
from backend.mollweide import *
from backend.font_scraping import *
import collections
import cv2
from backend.pack import run
import math


def scrape_links(request):
    from artscraper_dj.backend.scrape_utils import OVERWRITE_LINK
    if OVERWRITE_LINK or not len(Link.objects.all()):
        timestamp = int(time.time())
        art_forum_url = 'https://www.artforum.com/guide/country=US&place=New%20York&show=active&page_id={0}'
        art_links = list(set(get_art_links(art_forum_url)))
        for l in art_links:
            rank = alexa.get_rank(l)
            type = "ARTFORUM"
            Link(timestamp=timestamp, type=type, url=l, rank=rank).save()
        alexa_links = get_alexa_top_links(len(art_links))
        for l in alexa_links:
            type = "ALEXA"
            Link(timestamp=timestamp, type=type, url=l[1], rank=l[0]).save()
    return HttpResponse("done..")


def scrape_screens(request):
    if not len(Screenshot.objects.all()):
        links = Link.objects.all()
    else:
        links = Link.objects.exclude(pk__in=Screenshot.objects.values('link_id'))

    print 'Undone links:', len(links)
    # speed up computations
    pool = ThreadPool(5)
    pool.map(site_saver.get_screenshot, links)
    return HttpResponse("done..")


def get_background_color(request):
    if not len(BackgroundColor.objects.all()):
        links = Link.objects.all()
    else:
        links = Link.objects.exclude(pk__in=BackgroundColor.objects.values('link_id'))
    print 'Undone links:', len(links)
    pool = ThreadPool(5)
    pool.map(site_saver.get_background_color, links)
    return HttpResponse("done..")


def get_height_map(request):
    all_links = Link.objects.all()
    for l in all_links:
        scr = HeightMap.objects.filter(link=l)
        if len(scr) > 1:
            scr[0].delete()
    print 'Removed duplicates...'
    print 'getting_height maps...'
    import backend.height_map_generator
    if not len(HeightMap.objects.all()):
        links = Link.objects.all()
    else:
        links = Link.objects.exclude(pk__in=HeightMap.objects.values('link_id'))
    print 'Undone links:', len(links)
    for l in links:
        print 'processing link: ', l.url
        url = l.url.replace('https://', '').replace('http://', '').replace('www.', '')
        hmap = backend.height_map_generator.generate(url)
        HeightMap(link=l, height_map=hmap).save()
    return HttpResponse("done..")


def get_dominant_colors(request):
    for obj in Screenshot.objects.all():
        print color_utils.dominant_colors(obj)


def get_island_grid_artforum(request):
    _get_grid("ARTFORUM")
    return _get_grid("ARTFORUM", store_keys=True)


def get_island_grid_alexa(request):
    _get_grid("ALEXA")
    return _get_grid("ALEXA", store_keys=True)


def _get_grid(type, store_keys=False):
    applied_links_id = set(Screenshot.objects.values_list('link_id', flat=True)).intersection(
        set(BackgroundColor.objects.values_list('link_id', flat=True)))
    l = sorted(Link.objects.filter(pk__in=applied_links_id, type=type), key=operator.attrgetter('rank'))
    backgrounds = []
    islands = []
    for i in l:
        backgrounds.append(BackgroundColor.objects.filter(link=i.pk)[0].background)
        islands.append(Island.objects.filter(link=i.pk)[0].island)
    walk = array_helper.get_walk(len(l))
    x, y = array_helper.get_dimensions(len(l))
    back_blurred = array_helper.concatenate_backgrounds(x, y, walk, backgrounds, l)
    print 'Blurred background done..'
    islands_concatenated = array_helper.concatenate_islands(x, y, walk, islands, l, store_keys=store_keys)
    print 'Islands concatenated..'
    islands_concatenated = run(islands_concatenated)
    print 'Islands resized..'
    islands_placed = array_helper.combine_img(back_blurred, islands_concatenated).convert('RGB')
    print 'Islands placed..'
    img_gradiented = add_gradient(islands_placed)
    print 'Gradient applied..'
    cloudy_img = add_clouds(img_gradiented)
    print 'Clouds applied..'
    cloudy_img.save('output-combined-{0}.png'.format(str(int(time.time()))))
    img = cloudy_img.resize((cloudy_img.size[0] / 4, cloudy_img.size[1] / 4))
    resized_fname = 'output-resized-{0}.png'.format(str(int(time.time())))
    img.save(resized_fname)
    get_mollweide(resized_fname)
    return HttpResponse("done..")


def generate_islands(request):
    applied_links_id = set(Screenshot.objects.values_list('link_id', flat=True)).intersection(
        set(BackgroundColor.objects.values_list('link_id', flat=True))).difference(
        set(Island.objects.values_list('link_id', flat=True)))

    def _generate(i):
        try:
            print 'current link:', i
            height_map = json.loads(HeightMap.objects.filter(link=i)[0].height_map)
            screen = Screenshot.objects.filter(link=i)[0]
            dominant_colors = sorted(color_utils.dominant_colors(screen), key=lambda x: x[0] + x[1] + x[2])
            img_bytes = mapper(height_map, dominant_colors)
            Island(link=Link.objects.get(pk=i), island=img_bytes).save()
        except:
            traceback.print_exc(10)
            print "Error with link: {0}".format(i)

    pool = ThreadPool(10)
    pool.map(_generate, applied_links_id)

    return HttpResponse("done..")


def generate_fragments(request):
    if not Segment.objects.count():
        screens_ids = set(Screenshot.objects.values_list('id', flat=True))
    else:
        screens_ids = set(Screenshot.objects.values_list('id', flat=True)).difference(
            Segment.objects.values_list('screenshot_id', flat=True))
    cnt = len(screens_ids)
    print 'Undone screenshots:', cnt

    def _generate(_id):
        # print "Remaining: {0}".format(str(cnt))
        screen = Screenshot.objects.get(pk=_id)
        tpe = Link.objects.get(pk=screen.link.id).type
        make_segments(screen, tpe)

    pool = ThreadPool(10)
    pool.map(_generate, screens_ids)
    return HttpResponse("done..")


def scrape_fonts(request):
    if not len(Fonts.objects.all()):
        links = Link.objects.all()
    else:
        links = Link.objects.exclude(pk__in=Fonts.objects.values('link_id'))
    pool = ThreadPool(20)
    pool.map(get_fonts, links)
    return HttpResponse("done..")


def get_max_height(links):
    screens = Screenshot.objects.filter(link__in=links)
    widths = []
    heights = []
    pool = ThreadPool(20)

    def get_params(sc):
        image = cv2.imdecode(np.frombuffer(sc.screenshot, np.uint8), 1)
        h, w, channels = image.shape
        heights.append(h)
        widths.append(w)

    pool.map(get_params, screens)
    return max(widths), max(heights)


def get_least_used_background_color(links):
    d = collections.defaultdict(int)
    colors = BackgroundColor.objects.filter(link__in=links)
    for c in colors:
        d[c.background] += 1
    min_occur = min(d.values())
    least_used = [k for k, v in d.iteritems() if v == min_occur]
    return random.choice(least_used)


def get_sentences(request):
    if not len(Sentence.objects.all()):
        links = Link.objects.all()
    else:
        links = Link.objects.exclude(pk__in=Sentence.objects.values('link_id'))

    print 'Undone links:', len(links)
    # speed up computations
    pool = ThreadPool(5)
    pool.map(site_saver.get_sentences, links)
    return HttpResponse("done..")


def get_opposite_web_artforum(request):
    type = "ARTFORUM"
    get_opposite_web(type)
    return HttpResponse("done..")


def get_opposite_web_alexa(request):
    type = "ALEXA"
    get_opposite_web(type)
    return HttpResponse("done..")


def get_opposite_web(type):
    links = Link.objects.filter(type=type).values_list('id')
    print "Links done.."
    w, h = get_max_height(links)
    print "Max width: {0},height: {1}".format(w, h)
    color = tuple(json.loads(get_least_used_background_color(links)))
    img = Image.new('RGB', (w, h), color=color)
    print "Filtering segments...."
    segments_screenshot_ids = Segment.objects.filter(type=type).values_list('screenshot_id', flat=True)
    d = collections.defaultdict(int)
    for s in segments_screenshot_ids:
        d[s] += 1
    max_fragments = max(d.values())
    print "MAX FRAGMENTS:{0}".format(max_fragments)
    segments_sizes = list(Segment.objects.filter(type=type).values_list('width', 'height'))
    least_common_sizes = get_least_common_sizes(segments_sizes, max_fragments)
    segments_positions = list(Segment.objects.filter(type=type).values_list('position_x', 'position_y'))
    least_common_positions = get_least_common_positions(segments_positions, max_fragments)
    print "Least_common_sizes:", least_common_sizes
    print "Least_common_positions:", least_common_positions
    for i, l_size in enumerate(least_common_sizes):
        print "Placing location {0}".format(i)
        segmentss = Segment.objects.filter(width=l_size[0], height=l_size[1])
        for s in segmentss:
            pos = least_common_positions[i]
            _im = Image.open(io.BytesIO(s.segment))
            img.paste(_im, pos)
            del _im

    img.save('test-segmentation.png')


def get_least_common_sentences_artforum(request):
    type = "ARTFORUM"
    get_least_common_sentences(type=type)
    return HttpResponse("done..")


def get_least_common_sentences_alexa(request):
    type = "ALEXA"
    get_least_common_sentences(type=type)
    return HttpResponse("done..")


def get_least_common_sentences(type):
    from markov.markov import generate_artforum, generate_alexa
    links = Link.objects.filter(type=type).values_list('id')
    print "Links done.."
    w, h = get_max_height(links)
    print "Max width: {0},height: {1}".format(w, h)
    img = Image.new('RGBA', (w, h), color=(0, 0, 0, 0))
    sentences_link_ids = list(Sentence.objects.filter(type=type).values_list('link_id'))
    d = collections.defaultdict(int)
    for s in sentences_link_ids:
        d[s] += 1
    max_sentences = max(d.values())
    print "MAX SENTENCES:{0}".format(max_sentences)
    sentence_positions = list(Sentence.objects.filter(type=type).values_list('pos_x', 'pos_y'))
    least_common_positions = get_least_common_positions(sentence_positions, max_sentences)
    if type == "ARTFORUM":
        txts = generate_artforum(max_sentences)
    else:
        txts = generate_alexa(max_sentences)
    fnt = ImageFont.truetype('/Users/admin/PycharmProjects/artscraper_dj/artscraper_dj/Rubik-Regular.ttf', 14)
    for i, txt in enumerate(txts):
        pos_x, pos_y = least_common_positions[i]
        d = ImageDraw.Draw(img)
        d.text((pos_x, pos_y), txt, font=fnt, fill=(0, 0, 0, 255))
    img.save('sentences-{0}.png'.format(type))


def get_least_used_font_artforum(request):
    type = "ARTFORUM"
    get_least_used_font(type)
    return HttpResponse("done..")


def get_least_used_font_alexa(request):
    type = "ALEXA"
    get_least_used_font(type)
    return HttpResponse("done..")


def dump_sentences_artforum(request):
    type = "ARTFORUM"
    with open('dictionary-artforum.txt', 'w') as output:
        links = Link.objects.filter(type=type).values_list('id')
        for s in Sentence.objects.filter(link__in=links):
            output.write(s.text.encode('utf-8'))
            output.write('\n')
    return HttpResponse("done..")


def dump_sentences_alexa(request):
    type = "ALEXA"
    with open('dictionary-alexa.txt', 'w') as output:
        links = Link.objects.filter(type=type).values_list('id')
        for s in Sentence.objects.filter(link__in=links):
            output.write(s.text.encode('utf-8'))
            output.write('\n')
    return HttpResponse("done..")


def parse_markov(request):
    from markov.markov import parse_alexa, parse_artforum
    parse_alexa()
    parse_artforum()
    return HttpResponse("done..")


def generate_markov(request):
    from markov.markov import generate_alexa, generate_artforum
    print generate_artforum(1000)
    return HttpResponse("done..")


def get_least_used_font(type):
    links = Link.objects.filter(type=type).values_list('id')
    print "Links done.."
    l = list(Fonts.objects.filter(link__in=links).values_list('font', flat=True))
    fonts = map(json.loads, l)
    fonts_flat = [item for sublist in fonts for item in sublist]
    d = collections.defaultdict(int)
    for f in fonts_flat:
        d[f.lower()] += 1
    min_occur = min(d.values())
    least_used = [k for k, v in d.iteritems() if v == min_occur]

    print "LEAST USED FONT: ", random.choice(least_used)


def get_least_common_sizes(sizes, count):
    length = len(sizes)
    mean_pixel = sum([pair[0] * pair[1] for pair in sizes]) / length
    result_set = []
    for size in sizes:
        result_set.append((size, abs(size[0] * size[1] - mean_pixel)))
    result_set.sort(key=lambda a: a[1], reverse=True)
    result_set = [a[0] for a in result_set]
    return result_set[:count]


def get_least_common_positions(positions, count):
    length = len(positions)
    mean_x = sum([pair[0] for pair in positions]) / length
    mean_y = sum([pair[1] for pair in positions]) / length
    result_set = []
    for pos in positions:
        result_set.append((pos, distance(pos, (mean_x, mean_y))))
    result_set.sort(key=lambda a: a[1], reverse=True)
    result_set = [a[0] for a in result_set]
    return result_set[:count]


def distance(p1, p2):
    return math.sqrt((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2)
