from django.db import models


class Link(models.Model):
    timestamp = models.IntegerField()
    type = models.CharField(max_length=50)
    url = models.CharField(max_length=400)
    rank = models.IntegerField()


class Screenshot(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    screenshot = models.BinaryField()


class BackgroundColor(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    background = models.CharField(max_length=50)


class HeightMap(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    height_map = models.TextField()


class Island(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    island = models.BinaryField()


class Fonts(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    font = models.TextField()


class Segment(models.Model):
    screenshot = models.ForeignKey(
        'Screenshot',
        on_delete=models.CASCADE)
    segment = models.BinaryField()
    position_x = models.IntegerField()
    position_y = models.IntegerField()
    width = models.IntegerField()
    height = models.IntegerField()
    type = models.CharField(max_length=50)


class Sentence(models.Model):
    link = models.ForeignKey(
        'Link',
        on_delete=models.CASCADE)
    text = models.TextField()
    pos_x = models.IntegerField()
    pos_y = models.IntegerField()
    type = models.CharField(max_length=50)
